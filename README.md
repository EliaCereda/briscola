Briscola Multiplayer
====================

Briscola Multiplayer è una versione web del classico gioco della Briscola a 4 giocatori che permette di giocare online dal browser. È stato sviluppato utilizzando Node.js e le più recenti tecnologie HTML5, Javascript e CSS3.

Installazione del server
------------------------

Per poter giocare è necessario installare il server di gioco su un computer a cui i giocatori si possano collegare. I passi da seguire sono i seguenti:

- **Installare Node.js:** scaricare il pacchetto di installazione dal [sito ufficiale][1] ed installarlo sul sistema
- **Scaricare il server:** il [pacchetto][2] con i file necessari al funzionamento del server deve essere scaricato e scompattato in una cartella del computer
- **Installazione delle dipendenze:** il server necessita di una serie di librerie per il funzionamento che possono essere installate eseguendo il seguente comando nel prompt dei comandi

```
$ cd <# percorso della cartella del server #>
$ npm install
```

- **Avvio del server:** è possibile avviare il programma del server eseguendo il seguente comando sullo stesso prompt dell'installazione

```
$ npm start
```

Avvio del gioco
---------------

Per iniziare il gioco uno dei giocatori deve connettersi al server usando il seguente URL:

```
http://<# indirizzo IP del server #>:3000/
```

Una volta essersi connessi e aver scelto un nickname bisogna creare una partita, scegliendo il numero di round da disputare e fornendo agli altri giocatori l'URL della partita che viene generato al gioco. Gli altri giocatori si collegano utilizzando questo URL e scelgono a loro volta il proprio nickname.

Una volta completata questa fase il gioco ha inizio.

Sviluppo
--------

I sorgenti del gioco sono _open source_ e messi a disposizione di tutti su [BitBucket][3] utilizzando il software di controllo di versione Git. Sulla [pagina del repository][4] è possibile vedere lo storico delle modifiche e contribuire allo sviluppo.

Crediti
-------

Questo gioco è stato sviluppato da Andrea Cattaneo e Elia Cereda.

© 2014-2015 — Tutti i diritti riservati.

[1]: http://nodejs.org
[2]: https://bitbucket.org/EliaCereda/briscola/get/stable.zip
[3]: https://bitbucket.org
[4]: https://bitbucket.org/EliaCereda/briscola