'use strict';

// Enable debug messages by default
process.env.DEBUG = process.env.DEBUG || 'briscola:*';

var debug = require('debug')('briscola:server');

var path = require('path');

var WebServer = require('./lib/web-server');
var GameServer = require('./lib/game-server');

module.exports = Server;

function Server(port, done) {
  var self = this;
  
  if (!(self instanceof Server)) return new Server(port, done);
  
  if (typeof(arguments[0]) == 'function') {
    port = undefined;
    done = arguments[0];
  }
  
  var http = self.http = require('http').Server();
  
  var webServer = self.webServer = new WebServer(path.join(__dirname, 'client'), http);
  var gameServer = self.gameServer = new GameServer(http);
  
  // Bind the HTTP server to a specific port
  http.listen(port, function () {
    var address = http.address();
    
    self.address = address.address;
    self.port = address.port;
    
    debug('Server listening on %s, port %d', self.address, self.port);
    
    done && done(self);
  });
}

Server.prototype.close = function (done) {
  this.http.close(function () {
    debug('Server closed');
    
    done && done();
  });
};

if(require.main === module) {
  var server = new Server(process.env.PORT || 3000);
}
