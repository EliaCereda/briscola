'use strict';

module.exports = {
  getRoomName: function (opts) {
    var roomName = [];

    if (opts.match) {
      roomName.push('match:' + opts.match.id);
    }

    if (opts.player) {
      roomName.push('player:' + opts.player.nickname);
    }

    return roomName.join('/');
  },
  
  joinRoom: function (socket, opts) {
    socket.join(this.getRoomName(opts));
  },
  
  leaveRoom: function (socket, opts) {
    socket.leave(this.getRoomName(opts));
  },
  
  broadcastTo: function (socket, opts) {
    if (socket.broadcast) {
      socket = socket.broadcast;
    }
    
    return socket.to(this.getRoomName(opts));
  }
};