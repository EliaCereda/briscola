'use strict';

var debug = require('debug')('briscola:game-server');

var express = require('express');
var errorHandler = require('errorhandler');
var path = require('path');

module.exports = WebServer;

function WebServer(root, server) {
  var app = this.app = express();
  
  debug('Webserver root is %s', root);
  
  // Return client.html for / and /:match 
  app.get(/^\/(\d+)?$/, function (req, res) {
    res.sendFile(path.join(root, 'index.html'));
  });

  // Serve static assets from the client/static/ folder
  app.use(express.static(path.join(root, 'static')));

  // Log server-side errors 
  app.use(errorHandler());
  
  // Attach the Express app as a request handler
  server.on('request', this.app);
}
