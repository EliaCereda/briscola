'use strict';

var debug = require('debug')('briscola:game-server');

var _ = require('underscore');
var SocketIO = require('socket.io');

var utils = require('./utils');

var Match = require('./models/match');
var Player = require('./models/player');
var Session = require('./models/session');

module.exports = GameServer;

function GameServer(server) {
  var self = this;

  var io = self.io = new SocketIO(server);

  // Create an object that doesn't inherit any method from Object
  self.players = Object.create(null);
  self.matches = [];

  io.on('connection', function (socket) {
    self.onClientConnected(socket);
  });
}

GameServer.prototype.onClientConnected = function (socket) {
  var self = this;

  var session = new Session(socket);
  
  socket.on('disconnected', function () {
    if (session.currentPlayer && session.currentMatch) {
      utils.broadcastTo(self.io, { match: session.currentMatch }).emit('player left', session.currentPlayer);
    }
  });
  
  socket.on('login', function (params, done) {
    if (session.isLoggedIn()) {
      done && done('Sei già loggato');
      return;
    }

    var nickname = params.nickname;

    if (!nickname || typeof(nickname) != 'string') {
      done && done('Nickname non valido');
      return;
    }

    // Create an object for the player if it doesn't exist
    if (!self.players[nickname]) {
      self.players[nickname] = new Player(nickname);
    }

    session.setCurrentPlayer(self.players[nickname]);

    // Login successful
    debug('Client %s logged in as %s', session, session.currentPlayer);
    done && done(null, session.currentPlayer);
  });

  socket.on('logout', function (params, done) {
    if (!session.isLoggedIn()) {
      done && done('Non sei loggato');
      return;
    }

    debug('Client %s logged out from %s', session, session.currentPlayer);
    
    if (session.currentMatch) {
      utils.broadcastTo(self.io, { match: session.currentMatch }).emit('player left', session.currentPlayer);
    }
      
    session.setCurrentMatch(null);
    session.setCurrentPlayer(null);

    done && done();
  });

  socket.on('create match', function (params, done) {
    if (!session.isLoggedIn()) {
      done && done('Non sei loggato');
      return;
    }

    var matchId = self.matches.length;

    // Save the new match
    var match = self.matches[matchId] = new Match(matchId, session.currentPlayer, params.numberOfRounds);

    match.on('state changed', function (oldState, newState) {
      switch(newState) {
        case 'match ready':
          utils.broadcastTo(self.io, { match: match }).emit('match ready', match);
          break;
      }
    });

    match.on('begin round', function (round) {
      utils.broadcastTo(self.io, { match: match }).emit('begin round', round);
    });

    match.on('begin turn', function (turn) {
      utils.broadcastTo(self.io, { match: match }).emit('begin turn', turn);
    });

    match.on('dealt cards', function (cardsDealt) {
      utils.broadcastTo(self.io, { match: match }).emit('dealt cards', cardsDealt);
    });

    match.on('current player changed', function (turn, player) {
      utils.broadcastTo(self.io, { match: match }).emit('current player changed', turn, player);
    });

    match.on('turn finished', function (turn) {
      utils.broadcastTo(self.io, { match: match }).emit('turn finished', turn);
    });

    match.on('round finished', function (round) {
      utils.broadcastTo(self.io, { match: match }).emit('round finished', round);
    });

    match.on('match finished', function (match) {
      utils.broadcastTo(self.io, { match: match }).emit('match finished', match);
    });

    // Add the current player to the match
    match.addPlayer(session.currentPlayer);

    // Mark it as the current match
    session.setCurrentMatch(self.matches[matchId]);

    debug('%s created and joined %s', session.currentPlayer, match);
    done && done(null, match);
  });

  socket.on('join match', function (params, done) {
    if (!session.isLoggedIn()) {
      done && done('Non sei loggato');
      return;
    }

    var matchId = params.matchId;

    if (typeof(matchId) != 'number') {
      done && done('Match id non valido');
      return;
    }

    var match = self.matches[matchId];

    if (!match) {
      done && done('Match non trovato');
      return;
    }

    if (match.addPlayer(session.currentPlayer) === false) {
      done && done('Impossibile unirsi al match');
      return;
    }

    session.setCurrentMatch(match);

    debug('%s joined %s', session.currentPlayer, match);

    utils.broadcastTo(session.socket, { match: match }).emit('player joined', { nickname: session.currentPlayer.nickname });

    done && done(null, match);
  });

  socket.on('throw card', function (params, done) {
    if (!session.isLoggedIn()) {
      done && done('Non sei loggato');
      return;
    }

    var player = session.currentPlayer;

    if (!session.hasJoinedMatch()) {
      done && done('Non ti sei unito ad un match');
      return;
    }

    var match = session.currentMatch;
    var round = match.getCurrentRound();

    if (match.matchState != 'match ready' || !round) {
      done && done('Il match non è ancora iniziato');
      return;
    }

    var cardCode = params.cardCode;

    if (typeof(cardCode) != 'string') {
      done && done('Codice della carta non valido');
      return;
    }

    var card = round.playerThrowsCard(player, cardCode);

    if (!card) {
      done && done('Non puoi giocare questa carta');
      return;
    }

    utils.broadcastTo(self.io, { match: match }).emit('card thrown', { player: player.nickname, card: card });

    done && done(null, card);
  });

  socket.on('begin next round', function (done) {
    if (!session.isLoggedIn()) {
      done && done('Non sei loggato');
      return;
    }

    var player = session.currentPlayer;

    if (!session.hasJoinedMatch()) {
      done && done('Non ti sei unito ad una partita');
      return;
    }

    var result = session.currentMatch.playerReadyForNextRound(player);

    if (!result) {
      done && done('Richiesta non valida');
    } else {
      done && done();
    }
  });
};
