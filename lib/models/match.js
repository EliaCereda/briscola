'use strict';

var debug = require('debug')('briscola:models:match');

var _ = require('underscore');
var assert = require('assert');
var events = require('events');
var util = require('util');

var CouplesPoints = require('./couples-points');
var Round = require('./round');

module.exports = Match;

function Match(id, hostingPlayer, numberOfRounds) {
  Match.super_.call(this);
  
  this.id = id;
  
  // Matches are created in the waiting players state
  this.matchState = 'waiting players';
  
  this.hostingPlayer = hostingPlayer;
  this.numberOfRounds = numberOfRounds || 3;
  this.players = [];
  this.rounds = [];
  
  this.points = null;
  
  this.matchFinished = false;
  this.winnerCoupleId = null;
  this.winnerCouple = null;
  
  this.waitingForPlayers = null;
}

util.inherits(Match, events.EventEmitter);

Match.prototype.addPlayer = function (player) {
  var players = this.players;
  
  if (this.matchState != 'waiting players') {
    return false;
  }
  
  if (_(players).contains(player)) {
    return false;
  }
  
  players.push(player);
    
  if (players.length == 4) {
    this.setMatchState('match ready');
  }
  
  // Return the index of the player
  return _(players).indexOf(player);
};

Match.prototype.allowedTransitions = {
    'waiting players': [ 'match ready' ],
    'match ready': []
};

Match.prototype.isAllowedTransition = function(source, target) {
  return _(this.allowedTransitions[source]).contains(target);
};

Match.prototype.setMatchState = function (newState) {
  var self = this;
  
  assert(self.isAllowedTransition(self.matchState, newState), 'Invalid match state transition');
  
  var oldState = self.matchState;
  self.matchState = newState;
  
  setImmediate(function () {
    self.matchStateWillChange(oldState, newState);
    
    self.emit('state changed', oldState, newState);
    
    debug('%s transitioned from state \'%s\' to state \'%s\'', self, oldState, newState);
  });
};

Match.prototype.matchStateWillChange = function (source, target) {
  switch (target) {
    case 'match ready':
      
      this.points = new CouplesPoints(this.players);
      
      this.nextRound();
      break;
  }
};

Match.prototype.nextRound = function () {
  var self = this;
  
  if (!self.isMatchFinished()) { 
    var roundId = self.rounds.length;

    var round = new Round(self, roundId, self.getPlayerOrderForNextRound());

    round.on('begin round', function () {
      self.emit('begin round', round);
    });
    
    round.on('begin turn', function (turn) {
      self.emit('begin turn', turn);
    });
    
    // Forward the event to the player
    round.on('dealt cards', function (cards) {
      self.emit('dealt cards', cards);
    });

    round.on('current player changed', function (turn, player) {
      self.emit('current player changed', turn, player);
    });

    round.on('turn finished', function (turn) {
      self.emit('turn finished', turn);
    });

    round.on('round finished', function () {
      if (!round.isDraw) {
        self.points.addPointsToCouple(round.winnerCouple, 1);
      }
      
      if (!self.isMatchFinished()) {
        self.waitingForPlayers = _(self.players).pluck('nickname');
        self.emit('round finished', round);
      } else {
        // Emit the "match finished" event
        setImmediate(function () {
          self.nextRound();
          self.emit('round finished', round);
        });
      }
      
      
    });

    self.rounds.push(round);
  } else {
    self.matchFinished = true;
    
    self.winnerCoupleId = self.points.getWinnerCoupleId();
    self.winnerCouple = self.points.getCouple(self.winnerCoupleId);
    
    var points = self.points.getPoints(self.winnerCoupleId);
    
    setImmediate(function () {
      debug('%s finished, %s won with %d points', self, self.winnerCouple.join(' and '), points);
      self.emit('match finished', self);
    });
  }
};

Match.prototype.isMatchFinished = function () {
  var maxPoints = this.points.getMaxPoints();
  var targetPoints = Math.floor(this.numberOfRounds / 2) + 1;
  
  return maxPoints >= targetPoints;
};

Match.prototype.getPlayerOrderForNextRound = function () {
  var currentRound = this.getCurrentRound();
  
  var playerOrder;
  
  if (!currentRound) {
    playerOrder = _(this.players).clone();
    
    // Choose the first player at random
    var numberOfRotations = Math.floor(Math.random() * 4);
    
    _(numberOfRotations).times(function () {
      playerOrder.push(playerOrder.shift());
    });
  } else {
    playerOrder = _(currentRound.playerOrder).clone();

    // Take the first player and make it last
    playerOrder.push(playerOrder.shift());
  }
  
  return playerOrder;
};

Match.prototype.playerReadyForNextRound = function (player) {
  var self = this;
  
  if (!self.waitingForPlayers) {
    return false;
  }
  
  var playerIndex = _(self.waitingForPlayers).indexOf(player.nickname);
  if (playerIndex == -1) {
    return false;
  }
  
  self.waitingForPlayers.splice(playerIndex, 1);
  
  debug('%s is ready for the next round', player);
  
  if (self.waitingForPlayers.length == 0) {
      setImmediate(function () {
        self.waitingForPlayers = null;
        
        self.nextRound();
      });
  }
  
  return true;
}

Match.prototype.getCurrentRound = function () {
  return _(this.rounds).last();
};

Match.prototype.toString = function () {
  return '[Match ' + this.id + ']';
};
