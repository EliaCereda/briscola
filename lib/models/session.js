'use strict';

var debug = require('debug')('briscola:models:session');

var utils = require('../utils');

module.exports = Session;

function Session(socket) {
  this.socket = socket;
  
  this.currentPlayer = null;
  this.currentMatch = null;
}

Session.prototype.isLoggedIn = function () {
  return this.currentPlayer !== null;
};

Session.prototype.setCurrentPlayer = function (player) {
  if (this.currentPlayer != player) {
    if (this.currentPlayer) {
      utils.leaveRoom(this.socket, { player: this.currentPlayer });
    }

    this.currentPlayer = player;

    if (this.currentPlayer) {
      utils.joinRoom(this.socket, { player: this.currentPlayer });
    }
  }
};

Session.prototype.hasJoinedMatch = function () {
    return this.currentMatch !== null;
};

Session.prototype.setCurrentMatch = function (match) {
  if (this.currentMatch != match) {
    if (this.currentMatch) {
      utils.leaveRoom(this.socket, { match: this.currentMatch });
      utils.leaveRoom(this.socket, { match: this.currentMatch, player: this.currentPlayer });
    }

    this.currentMatch = match;

    if (this.currentMatch) {
      utils.joinRoom(this.socket, { match: this.currentMatch });
      utils.joinRoom(this.socket, { match: this.currentMatch, player: this.currentPlayer });
    }
  }
};

Session.prototype.toString = function () {
  return '[Session ' + this.socket.id + ']';
};