'use strict';

var debug = require('debug')('briscola:models:player');

module.exports = Player;

function Player(nickname) {
  this.nickname = nickname;
}

Player.prototype.toString = function () {
  return '[Player ' + this.nickname + ']';
};
