'use strict';

var debug = require('debug')('briscola:models:turn');

var _ = require('underscore');
var async = require('async');
var events = require('events');
var util = require('util');

module.exports = Turn;

function Turn(round, id, playerOrder) {
  var self = this;
  
  self.round = round;
  self.id = id;
  
  debug('Created %s', self);
  
  self.playerOrder = playerOrder;
  self.currentPlayer = null;
  
  self.thrownCards = [];
  
  self.turnFinished = false;
  
  self.winnerPlayer = null;
  self.winnerCard = null;
  
  self.points = null;
  
  setImmediate(function () {
    self.emit('begin turn', self);
    self.nextPlayer();
  });
}

util.inherits(Turn, events.EventEmitter);

Turn.prototype.nextPlayer = function () {
  var self = this;
  
  var nextPlayer;
  
  if (!self.currentPlayer) {
    nextPlayer = _(self.playerOrder).first();
  } else {
    var currentIndex = _(self.playerOrder).indexOf(self.currentPlayer);
    
    nextPlayer = self.playerOrder[currentIndex + 1];
  }
  
  if (nextPlayer) {
    self.currentPlayer = nextPlayer;
    
    setImmediate(function () {
      debug('Current player in %s is now %s', self, self.currentPlayer);
      self.emit('current player changed', self, self.currentPlayer);
    });
  } else {
    // There are no more players, find out who won
    var winner = self.findWinner();

    self.turnFinished = true;
    self.winnerPlayer = winner.player;
    self.winnerCard = winner.card;

    self.points = self.calculatePoints();
    
    debug('%s finished and %s won %d points with card %s', self, self.winnerPlayer, self.points, self.winnerCard);
    self.emit('turn finished', self);
  }
};

Turn.prototype.findWinner = function () {
  var self = this;
  
  var briscola = self.round.briscola;
  
  return _(self.thrownCards).reduce(function (current, next) {
    var currentCard = current.card;
    var nextCard = next.card;
    
    var isCurrentBriscola = (currentCard.suit == briscola.suit);
    var isNextBriscola = (nextCard.suit == briscola.suit);
    
    // If both cards are briscole, the card with the highest value wins.
    if (isCurrentBriscola && isNextBriscola) {
      return _([ current, next ]).max(function (each) { return each.card.getSortingValue(); });
    }
    
    // If one is a briscola and the other isn't, the briscola wins.
    if (isCurrentBriscola) {
      return current;
    }
    
    if (isNextBriscola) {
      return next;
    }
    
    // If the suit is the same, the card with the highest value wins
    if (current.card.suit == next.card.suit) {
      return _([ current, next ]).max(function (each) { return each.card.getSortingValue(); });
    }
    
    // Else, the current card wins
    return current;
  });
};

Turn.prototype.calculatePoints = function () {
  return _(this.thrownCards).reduce(function (sum, next) {
    return sum + next.card.points;
  }, 0);
};

Turn.prototype.toJSON = function () {
  var clone = _(this).clone();
  
  clone.round = clone.round.id;
  
  return clone;
};

Turn.prototype.toString = function () {
  return '[Turn ' + this.round.match.id + '.' + this.round.id + '.' + this.id + ']';
};
