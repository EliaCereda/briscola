'use strict';

var debug = require('debug')('briscola:models:card');

var _ = require('underscore');
var assert = require('assert');

module.exports = Card;

function Card(suit, value) {
  assert(_(Card.suits).has(suit), 'Invalid suit');
  assert(_(Card.values).has(value), 'Invalid value');

  this.suit = suit;
  this.value = value;

  this.name = Card.values[value].name;
  this.points = Card.values[value].points;

  this.code = Card.suits[suit].code + this.value;
}

Card.suits = {
  ori: { code: 'A' },
  coppe: { code: 'B' },
  bastoni: { code: 'C' },
  spade: { code: 'D' }

};

Card.values = {
  1:  { name: 'asso', points: 11 },
  2:  { name: 'due', points: 0 },
  3:  { name: 'tre', points: 10 },
  4:  { name: 'quattro', points: 0 },
  5:  { name: 'cinque', points: 0 },
  6:  { name: 'sei', points: 0 },
  7:  { name: 'sette', points: 0 },
  8:  { name: 'fante', points: 2 },
  9:  { name: 'cavallo', points: 3 },
  10: { name: 're', points: 4 }
};

Card.prototype.getSortingValue = function () {
  // Asso, tre, re, cavallo and fante beat the other cards
  if (this.points) {
    return this.points * 10;
  } else {
    return this.value;
  }
};

Card.prototype.toString = function () {
  return '[Card ' + this.name + ' di ' + this.suit + ']';
};
