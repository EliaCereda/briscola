'use strict';

var debug = require('debug')('briscola:models:couples-points');

var _ = require('underscore');
var assert = require('assert');

module.exports = CouplesPoints;

function CouplesPoints(players) {
  this.couples = _(players).groupBy(function (player, index) { return index % 2; });
  this.points = _(this.couples).map(function () { return 0; });
}

CouplesPoints.prototype.indexOfCoupleContainingPlayer = function (player) {
  var foundIndex = null;
  
  _(this.couples).each(function (couple, index) {
    if (_(couple).contains(player)) {
      foundIndex = index;
    }
  });
  
  return foundIndex;
};

CouplesPoints.prototype.addPointsToCouple = function (coupleId, points) {
  var self = this;
  
  self.points[coupleId] += points;
  
  _(self.couples).each(function (couple, index) {
    debug('%s have %d points', couple.join(' and '), self.points[index]);
  });
};

CouplesPoints.prototype.addPointsToPlayer = function (player, points) {
  this.addPointsToCouple(this.indexOfCoupleContainingPlayer(player), points);
};

CouplesPoints.prototype.getCouple = function (coupleId) {
  return this.couples[coupleId];
};

CouplesPoints.prototype.getPoints = function (coupleId) {
  return this.points[coupleId];
};

CouplesPoints.prototype.getWinnerCoupleId = function () {
  if (this.points[0] == this.points[1]) {
    return null;
  }
  
  if (this.points[0] > this.points[1]) {
    return 0;
  }
  
  if (this.points[0] < this.points[1]) {
    return 1;
  }
};

CouplesPoints.prototype.getMaxPoints = function () {
  return _(this.points).max();
};
