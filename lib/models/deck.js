'use strict';

var verbose = require('debug')('briscola-verbose:models:deck');

var _ = require('underscore');

var Card = require('./card');

module.exports = Deck;

function Deck() {
  this.cards = null;

  this.generateCards();
}

Object.defineProperty(Deck.prototype, 'numberOfCards', {
  enumerable: true,
  
  get: function () {
    return this.cards.length;
  }
});

Deck.prototype.generateCards = function () {
  var self = this;

  var cards = _(Card.suits).map(function (suitParams, suit) {
    return _(Card.values).map(function (valueParams, value) {
      return new Card(suit, value);
    });
  });

  self.cards = _(cards).chain().flatten().shuffle().value();
};

Deck.prototype.pickCard = function () {
  var card = this.cards.pop();

  verbose('Picked card %s from %s', card, this);

  return card;
};

Deck.prototype.toJSON = function () {
  return _(this).pick('numberOfCards');
};

Deck.prototype.toString = function () {
  return '[Deck - ' + this.cards.length + ' cards]';
};
