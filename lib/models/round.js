'use strict';

var debug = require('debug')('briscola:models:round');

var _ = require('underscore');
var events = require('events');
var util = require('util');

var CouplesPoints = require('./couples-points');
var Deck = require('./deck');
var Turn = require('./turn');

module.exports = Round;

function Round(match, id, playerOrder) {
  var self = this;
  
  self.match = match;
  self.id = id;
  
  debug('Created %s', self);
  
  self.deck = new Deck();
  self.playerOrder = playerOrder;
  self.playerData = {};
  self.turns = [];
  
  _(playerOrder).each(function (player) {
    self.playerData[player.nickname] = {
      hand: []
    };
  });
  
  // Store the points of each couple
  self.points = new CouplesPoints(match.players);
  
  self.roundFinished = false;
  self.isDraw = null;
  self.winnerCouple = null;
  
  
  self.briscolaHasBeenDealt = false;
  
  setImmediate(function () {
    self.nextTurn();
    
    self.briscola = self.deck.pickCard();
    debug('Briscola for %s is %s', self, self.briscola);
    
    self.emit('begin round', self);
  });
}

util.inherits(Round, events.EventEmitter);

Round.prototype.nextTurn = function () {
  var self = this;
  
  if (self.turns.length < 10) {
    var turnId = self.turns.length;
    var turn = new Turn(self, turnId, self.getPlayerOrderForNextTurn());

    turn.on('begin turn', function () {
      self.emit('begin turn', turn);
    });
    
    turn.on('current player changed', function (turn, player) {
      self.emit('current player changed', turn, player);
    });

    turn.once('turn finished', function () {
      self.points.addPointsToPlayer(turn.winnerPlayer, turn.points);
      self.emit('turn finished', turn);
      
      var turnDelay = process.env.TURN_DELAY || 3500;
      
      setTimeout(function () {
        self.nextTurn();
      }, turnDelay);
    });

    self.turns.push(turn);

    self.dealCards();
  } else {
    self.roundFinished = true;
    self.winnerCouple = self.points.getWinnerCoupleId();
    
    if (self.winnerCouple !== null) {
      self.isDraw = false;
      
      var couple = self.points.getCouple(self.winnerCouple);
      var points = self.points.getPoints(self.winnerCouple);
      debug('%s finished, %s won with %d points', this, couple.join(' and '), points);
    } else {
      self.isDraw = true;
      debug('%s finished with a draw', this);
    }
    
    self.emit('round finished', this);
  }
};

Round.prototype.getPlayerOrderForNextTurn = function () {
  var currentTurn = this.getCurrentTurn();
  
  if (!currentTurn) {
    return _(this.playerOrder).clone();
  }
  
  var playerOrder = currentTurn.playerOrder;
  
  // The first player in a turn is who won the previous one
  // Take the first player and make it last until this happens
  while(currentTurn.winnerPlayer != _(playerOrder).first()) {
    playerOrder.push(playerOrder.shift());
  }
  
  return playerOrder;
};

Round.prototype.dealCards = function () {
  var self = this;
  
  if (!self.briscolaHasBeenDealt) {
    var cardsDealt = {};

    // Deal cards following the player order
    _(self.getCurrentTurn().playerOrder).each(function (player) {
      var data = self.playerData[player.nickname];

      var cardsToDeal = 3 - data.hand.length;
      var cards = [];

      for (var i = 0; i < cardsToDeal; i++) {
        var card = self.deck.pickCard();

        // The last card to deal is the briscola
        if (!card) {
          card = self.briscola;
          self.briscolaHasBeenDealt = true;
        }

        data.hand.push(card);
        cards.push(card);
      }

      cardsDealt[player.nickname] = cards;

      debug('Dealt cards %s to %s', cards.join(', '), player);
    });
    
    cardsDealt.briscolaHasBeenDealt = self.briscolaHasBeenDealt;
    
    setImmediate(function () {
      self.emit('dealt cards', cardsDealt);
    });
  }
};

Round.prototype.getCurrentTurn = function () {
  return _(this.turns).last();
};

Round.prototype.playerThrowsCard = function (player, cardCode) {
  var self = this;
  
  var turn = self.getCurrentTurn();
  
  if (turn.currentPlayer.nickname != player.nickname) {
    return false;
  }
  
  var playerHand = self.playerData[player.nickname].hand;
  
  var cardIndex = null;
  for (var i = 0; i < playerHand.length; i++) {
    if (playerHand[i].code == cardCode) {
      cardIndex = i;
      break;
    }
  }
  
  if (cardIndex === null) {
    return false;
  }
  
  var card = playerHand[cardIndex];
  
  // Remove the card from the array
  playerHand.splice(cardIndex, 1);
  
  turn.thrownCards.push({
    player: player,
    card: card
  });
  
  debug('%s threw %s', player, card);
  
  setImmediate(function () {
    turn.nextPlayer();
  });
  
  return card;
};

Round.prototype.toJSON = function () {
  var clone = _(this).clone();
  
  clone.match = this.match.id;
  
  // Let's cheat a little bit :P
  clone.matchFinished = this.match.matchFinished;
  
  return clone;
};

Round.prototype.toString = function () {
  return '[Round ' + this.match.id + '.' + this.id + ']';
};
