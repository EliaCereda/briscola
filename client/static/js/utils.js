//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex
if (!Array.prototype.findIndex) {
  Array.prototype.findIndex = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return i;
      }
    }
    return -1;
  };
}

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}

function hidePlayerCards() {
  $('.player-hand > .card').hideCard();
}

function getCardFileName(card) {
  if (card) {
    return URI.expand('/images/{cardCode}.png', { cardCode: card.code }).toString();
  } else {
    return "/images/back.png";
  }
}

function setCard(container, card) {
	$(container).attr('src', getCardFileName(card));
	$.data(container, "card", card);
}

/*function showBriscola(match) {
  var divBriscola = $(".briscola");
  //TODO aggiungere supporto per più round
  var cardBriscola = match.rounds[0].briscola;
  jQuery.data(divBriscola[0], "card", cardBriscola);
  divBriscola.attr("src", getCardFileName(cardBriscola));
}*/

function showBriscola(briscola) {
  var briscolaContainer = $(".briscola");
  jQuery.data(briscolaContainer, "card", briscola);
  briscolaContainer.attr("src", getCardFileName(briscola));
}


function showPlayerCards(match) {
  var divCards = $(".player-bottom").children(".card").toArray();
  //TODO aggiungere supporto per più round
  var myCards = match.rounds[0].playerData[localStorage['nick']].hand;
  for (var i = 0; i < 3; i++) {
    jQuery.data(divCards[i], "card", myCards[i]);
    $(divCards[i]).attr("src", getCardFileName(myCards[i]));
  }
}

function showPlayersNames(round) {
  //TODO aggiungere supporto per più round
  var players = round.playerOrder;

  var myPosition = players.findIndex(function(element, index, array) {
    return element.nickname == localStorage['nick'];
  });


  players[getIndex(0, myPosition, players.length)].container = $('.player-bottom');
  players[getIndex(1, myPosition, players.length)].container = $('.player-right');
  players[getIndex(2, myPosition, players.length)].container = $('.player-top');
  players[getIndex(3, myPosition, players.length)].container = $('.player-left');

  players.forEach(function(value) {
    value.container.children('h3').children('.player-name').text(value.nickname);
  });

  /*
  $('.player-bottom > h3 > .player-name').text(players[getIndex(0, myPosition, players.length)]);
  $('.player-right > h3 > .player-name').text(players[getIndex(1, myPosition, players.length)]);
  $('.player-top > h3 > .player-name').text(players[getIndex(2, myPosition, players.length)]);
  $('.player-left > h3 > .player-name').text(players[getIndex(3, myPosition, players.length)]);
  */
  return players;

}

function onError(err) {
  $('.game-board').addClass('hidden');
  alert(err);
}

//TODO trovare un nome più intelligente per questa funzione
function getIndex(indexRequired, startPosition, length) {
  return (startPosition + indexRequired) % length;
}

$.fn.isCardHidden = function () {
  return this.filter('.card-hidden');
};

$.fn.notCardHidden = function () {
  return this.not('.card-hidden');
};

$.fn.hideCard = function () {
  return this
    .attr('src', '/images/placeholder.png')
    .addClass('card-hidden');
};

$.fn.showCard = function () {
  return this.removeClass('card-hidden');
};
