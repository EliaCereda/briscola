var RoundResultModal = new function () {
  var modal = $('#modal-end-turn');

  this.addRound = function (round) {
    var points = round.points;
    
    modal.find('.total-header').before('<th>Round ' + (round.id + 1) + '</th>');
    
    modal.find('.couple-info').each(function (i) {
      var $this = $(this);
      
      // A couple wins if it has more points than the other
      var isRoundWinner = (points.points[i] > points.points[(i + 1) % 2]);
      
      var couplePoints = points.points[i];
      var coupleNames = points.couples[i]
        .map(function (player) {
          var name = player.nickname;

          if (name == localStorage['nick']) {
            name = '<strong>' + name + '</strong>';
          }

          return name;
        })
        .join(' e ');
      var coupleTotal = parseInt($this.find('.couple-total').text());
      
      if (isRoundWinner) {
        couplePoints = '<span class="label label-success">' + couplePoints + '</span>';
        
        coupleTotal++;
      } else {
        couplePoints = '<span class="label label-danger">' + couplePoints + '</span>';
      }
      
      $this.find('.couple-players').html(coupleNames);
      $this.find('.couple-total').before('<td>' + couplePoints + '</td>');
      $this.find('.couple-total').text(coupleTotal);
    });
    
    if (!round.matchFinished) {
      modal.find('.modal-title').text('Riepilogo round');
      modal.find('.modal-footer').show();
      
      modal.find('.match-result-message').hide();
    } else {
      modal.find('.modal-title').text('Riepilogo partita');
      modal.find('.modal-footer').hide();
      
      var winnerId = null;
      var winnerPlayers = null;
      var winnerTotal = 0;
      
      modal.find('.couple-info').each(function (id) {
        var $this = $(this);
        
        var players = $this.find('.couple-players').text();
        var total = parseInt($this.find('.couple-total').text());
        
        if (total > winnerTotal) {
          winnerId = id;
          winnerPlayers = players;
          winnerTotal = total;
        }
      });
      
      var winnerCouple = modal.find('.couple-info')[winnerId];
      $(winnerCouple).addClass('success');
      
      var isMatchWinner = (winnerPlayers.indexOf(localStorage['nick']) != -1);
      
      modal.find('.match-results-message').show();
      modal.find('.match-results-message h1').text(isMatchWinner ? 'Hai vinto!' : 'Hai perso :(');
    }
  }
  
  this.show = function (points, callback) {
    if (!modal.hasClass('in')) {

      $('#btn-next-round').one('click', function() {
        $(this).button('loading');
        callback();
      });

      modal.modal();
    }
  };

  this.hide = function(callback) {
    if (callback) {
      modal.one('hidden.bs.modal', function (e) {
        callback();
      });
    }

    modal.modal('hide');
    $('#btn-next-round').button('reset');
  };
}
