function showCreateMatchModal(callback) {
  var createMatchModal = $('#modalMatches');

  createMatchModal.modal();
  $('#form-create-match').one('submit', function( event ) {
    	  event.preventDefault();
    	  socket.emit('create match', { numberOfRounds: $('#number-of-rounds').val() },
    	  function(err, match) {
    	    createMatchModal.modal('hide');
    	    callback(err, match);
    	  });
  	 });

}

function showWaitPlayersModal(match, callback) {
  var createMatchModal = $('#modal-wait-players');

  var matchURL = URI.expand('/{matchId}', {matchId: match.id}).absoluteTo(document.location).toString();

  $('#match-url').val(matchURL);
  History.pushState({ matchId: match.id }, "Briscola", matchURL);

  console.log(match);
  match.players.forEach(printPlayerInWaitingPlayersList);



  window.socket.on('player joined', function (player) {
    $('#players-list').append('<li class="list-group-item">' + player.nickname + '</li>');
  });

  window.socket.once('match ready', function(match) {
    window.socket.off('player joined');
    createMatchModal.modal('hide');

    callback(match);
  });

  createMatchModal.modal();
}

function printPlayerInWaitingPlayersList(player) {
  $('#players-list').append('<li class="list-group-item">' + player.nickname + '</li>');
}
