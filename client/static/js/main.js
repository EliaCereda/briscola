jQuery(function ($) {
  'use strict';

  var players;

  var autoplay = false; //settandola a true, verranno giocate automaticamente le carte
  var delay = 0;
  var preload = true;


  window.socket = io.connect();
  window.socket.on('disconnect', function () {
    onError('Si è verificato un errore di comunicazione con il server. Ora sei disconesso');
  });


  login(function(err, player) {
    if (err) {
      onError(err);
    } else {
      var currentURL = URI(window.location);
      var rootURL = URI('/').absoluteTo(currentURL);

      if (currentURL.equals(rootURL)) {
        showCreateMatchModal(function(err, match) {
          if (err){
            onError(err);
          } else {
            showWaitPlayersModal(match, onMatchStarted);
          }
        });

      } else {
        var id = currentURL.filename();
        socket.emit('join match', { matchId: new Number(id) }, function(err, match) {

          if (err) {
            onError(err);
          } else {
            showWaitPlayersModal(match, onMatchStarted);
          }

        });
      }
    }


  });

  function onMatchStarted(match) {
     $(".game-board").removeClass("hidden");
  }

  socket.on("current player changed", function(turn, player) {

    var newCurrentPlayer = players.find(function(element, index, array) {
      return element.nickname == player.nickname;
    });

    newCurrentPlayer.container.addClass('current-player');

    if (newCurrentPlayer.nickname == localStorage['nick']) {
      if (autoplay) {
        setTimeout(function () {
          onCardClick.call(newCurrentPlayer.container.children('.card').notCardHidden()[0])
        }, delay);
      } else {
        newCurrentPlayer.container.children('.card').each(function( i ) {
          $(this).on('click', onCardClick);
        });
      }
    }
  });

  socket.on('card thrown', function(param) {
    $('.current-player').removeClass('current-player');

    var playerAffected = players.find(function(element, index, array) {
      return element.nickname == param.player;
    });

    var cardThrownContainer = $('<img class="card">')
                                .attr('src', getCardFileName(param.card))
                                .tooltip({
                                  placement: 'top',
                                  title: 'Giocata da ' + param.player
                                });

    $('.thrown-cards').append(cardThrownContainer);

    // Recalculate the size of the game board
    adjustGameBoard();

    if (param.player != localStorage['nick']) {
      $(playerAffected.container.children('.card').notCardHidden()[0]).hideCard();
    }

});

socket.on('dealt cards', function(dealtCards) {
  for (var i = 0; i < players.length; i++) {
    var player = players[i];
    var cards = dealtCards[player.nickname];

    for (var j = 0; j < cards.length; j++) {
      var cardElement = player.container.children('.card').isCardHidden()[0];

      if (player.nickname == localStorage['nick']) {
        setCard(cardElement, cards[j]);
      } else {
        setCard(cardElement, null);
      }

      // Remove the card element from the DOM and re-append it at the end
      // so that the new card is the rightmost one
      $(cardElement).detach();

      if (player.container.is('.player-top')) {
        player.container.children('h3').before(cardElement);
      } else {
        player.container.append(cardElement);
      }

      $(cardElement).showCard();
    }
  }

  if (dealtCards.briscolaHasBeenDealt) {
    $('.deck').addClass('empty');
  } else {
    $('.deck').removeClass('empty');
  }
});

socket.on('turn finished', function(turn) {
  $('.thrown-cards').addClass('turn-finished');

  $('.thrown-cards .card[src="' + getCardFileName(turn.winnerCard) + '"]')
    .addClass('highlight')
    .tooltip('show');
})

socket.on('begin turn', function(turn) {
  $('.thrown-cards').removeClass('turn-finished');

  $('.thrown-cards > .card')
    .tooltip('hide')
    .remove();
});

socket.on('begin round', function(round) {
  RoundResultModal.hide();

  players = showPlayersNames(round);
  showBriscola(round.briscola);
  hidePlayerCards();
});

socket.on('round finished', function(round){
  // Hide the tooltips when showing the modal with the results
  $('.thrown-cards > .card').tooltip('hide');

  RoundResultModal.addRound(round);

  RoundResultModal.show(round.points, function() {
    socket.emit('begin next round', function(err) {
      if (err) onError(err);
    });
  });
});

  socket.on('player left', function (player) {
    onError('Il giocatore ' + player.nickname + ' ha abbandonato la partita.');
  });
  
  function onCardClick() {
    $('.current-player').removeClass('current-player');

    $('.player-bottom > .card').off('click');
    socket.emit('throw card', { cardCode: jQuery.data(this, "card").code }, function(err) {
      if (err) onError(err);
    });

    $(this).hideCard();
  }

  var playerHands = $(".player-left, .player-right");
  var thrownCards = $(".thrown-cards");
  var $window = $(window);

  function adjustGameBoard() {
    playerHands.width($window.height());
    playerHands.css("top", -1 * playerHands.height());

    thrownCards.css("top", ($window.height() / 2) - (thrownCards.height() / 2));
  }


  adjustGameBoard();
  $(window).resize(function () { adjustGameBoard() });

  // Disable dragging images
  $('.game-board').on('dragstart', function(event) { event.preventDefault(); });

  // Enable tooltips
  $('[data-toggle="tooltip"]').tooltip();

  if (preload) {
    // Preload the images of the UI
    var images = [ '/images/back.png', '/images/placeholder.png' ];

    var suits = [ 'A', 'B', 'C', 'D' ];
    var values = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

    for (var i in suits) {
      if (values.hasOwnProperty(i)) {
        for (var j in values) {
          if (values.hasOwnProperty(j)) {
            images.push('/images/'+ suits[i] + values[j] + '.png');
          }
        }
      }
    }

    $.preload(images, 6);
  }

});
