function login(callback) {

   var nick = localStorage['nick'];

	  if (!nick) {
  	 showLoginPrompt(function (err, nickname) {
  	   if (!err) {
  	     nick = nickname;
  	   }
  	   eseguiLogin(nick, function(err, player) {
  	     localStorage['nick'] = player.nickname;
  	     waitingDialog.hide();
  	     callback(err, player);
  	   });
  	 });
  	} else {
  	  eseguiLogin(nick, function(err, player) {
  	    waitingDialog.hide();
  	     callback(err, player);
  	   });
  	}

  }


  function eseguiLogin(nick, callback) {
    waitingDialog.show();
    socket.emit('login', { nickname: nick }, function(err, player) {
  	  if (!err) {
  	    localStorage['nick'] = nick;
  	  }
  	  waitingDialog.hide();
  	  callback(err, player);
    });

  }

  function showLoginPrompt(callback) {
    $('#modalLogin').modal();

    $('#form-login').one('submit', function( event ) {
      event.preventDefault();

      $('#modalLogin').modal('hide');

      callback(null, $('#userNameLogin').val());
  	 });
  }
