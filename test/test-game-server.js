'use strict';

var _ = require('underscore');
var async = require('async');
var io = require('socket.io-client');
var should = require('should');

describe('Game server', function () {
  var server, clients;
  
  // Disable the delay between one turn and the next
  process.env.TURN_DELAY = 0;
  
  before('start the server', function (done) {
    // The server can take some time to start up
    this.timeout(5000);

    server = require('../server')(function () {
      function Client(nickname) {
        this.nickname = nickname;
        this.socket = io.connect('http://localhost:' + server.port, { 'force new connection': true });

        this.currentPlayer = null;
        this.currentMatch = null;

        this.hand = [];
      }

      clients = [
        new Client('player1'),
        new Client('player2'),
        new Client('player3'),
        new Client('player4')
      ];

      // Prepare the event handler for later
      setupCurrentPlayerChanged();

      done();
    });
  });

  after('close the server', function () {
    server.close();
  });

  it('should be online', function (done) {
    this.timeout(5000);

    async.each(clients, function (client, done) {
      client.socket.on('connect', function () {
        done();
      });
    }, done);
  });

  it('should not allow a login without a nickname', function (done) {
    clients[0].socket.emit('login', { nickname: '' }, function (err, player) {
      should(err).be.ok;

      should(player).not.be.ok;

      done();
    });
  });

  it('should handle login', function (done) {
    async.each(clients, function (client, done) {
      client.socket.emit('login', { nickname: client.nickname }, function (err, player) {
        should(err).not.be.ok;

        player.should.be.ok;
        player.should.have.property('nickname', client.nickname);

        client.currentPlayer = player;

        done();
      });
    }, done);
  });

  it('should not allow multiple logins', function (done) {
    clients[0].socket.emit('login', { nickname: 'fail' }, function (err, player) {
      err.should.be.ok;

      should(player).not.be.ok;

      done();
    });
  });

  it('should create matches', function (done) {
    var numberOfRounds = process.env.NUMBER_OF_ROUNDS || 5;
    
    clients[0].socket.emit('create match', { numberOfRounds: numberOfRounds }, function (err, match) {
      should(err).not.be.ok;

      match.should.be.ok;
      match.should.have.property('id').which.is.a.Number;
      match.should.have.property('matchState', 'waiting players');
      match.should.have.property('hostingPlayer', clients[0].currentPlayer);
      match.should.have.property('numberOfRounds', numberOfRounds);
      match.should.have.property('players').which.containEql(clients[0].currentPlayer);

      clients[0].currentMatch = match;

      done();
    });
  });

  it('should let players join matches', function (done) {
    var joiningClient = clients[1];
    var matchToJoin = clients[0].currentMatch;

    joiningClient.socket.emit('join match', { matchId: matchToJoin.id }, function (err, match) {
      should(err).not.be.ok;

      match.should.have.property('id', matchToJoin.id);
      match.should.have.property('players').which.containEql(joiningClient.currentPlayer);

      joiningClient.currentMatch = match;

      done();
    });
  });

  it('should emit an event when a player joins a match', function (done) {
    var joiningClient = clients[2];
    var matchToJoin = clients[0].currentMatch;

    async.each([ clients[0], clients[1] ], function (client, done) {
      client.socket.once('player joined', function (player) {
        player.should.be.eql(joiningClient.currentPlayer);

        done();
      });
    }, done);

    joiningClient.socket.emit('join match', { matchId: matchToJoin.id }, function (err, match) {
      joiningClient.currentMatch = match;
    });
  });

  it('should emit a \'match ready\' event when the fourth player joins', function (done) {
    var joiningClient = clients[3];
    var matchToJoin = clients[0].currentMatch;

    async.each(clients, function (client, done) {
      client.socket.once('match ready', function (match) {
        match.should.be.ok;
        match.should.have.property('id', client.currentMatch.id);
        match.should.have.property('matchState', 'match ready');
        match.should.have.property('players').which.containEql(client.currentPlayer);
        match.should.have.property('rounds').which.is.an.Array.with.length(1);

        var round = match.rounds[0];

        round.should.be.ok;
        round.should.have.property('id', 0);
        round.should.have.property('deck');
        round.should.have.property('playerOrder')
          .which.containEql(client.currentPlayer).and.is.an.Array.with.length(4);
        round.should.have.property('playerData');
        round.should.have.property('turns').which.is.an.Array;

        var deck = round.deck;

        deck.should.be.ok;
        deck.should.have.property('numberOfCards').which.is.a.Number;
        deck.should.not.have.property('cards');

        var playerData = round.playerData[client.currentPlayer.nickname];

        playerData.should.be.ok;
        playerData.should.have.property('hand').which.is.an.Array;

        done();
      });
    }, done);

    joiningClient.socket.emit('join match', { matchId: matchToJoin.id }, function (err, match) {
      joiningClient.currentMatch = match;
    });
  });

  // 'current player changed' is raised immediately after 'match ready', so the event listener
  // must be attached outside of the it() block. This fuction is called in before().

  // This is a hack to notify mocha when the test finishes
  var notiyTestDidFinish;

  function testDidFinish() {
    if (notiyTestDidFinish) {
      notiyTestDidFinish();
    } else {
      // If mocha hasn't yet executed the it() block, try again in 100ms
      setTimeout(testDidFinish, 100);
    }
  }

  function setupCurrentPlayerChanged() {
    async.each(clients, function (client, done) {
      client.socket.on('current player changed', function (turn, player) {
        if (player.nickname == client.nickname) {
          // Choose a card...
          var card = client.hand.pop();

          // ...and throw it
          client.socket.emit('throw card', { cardCode: card.code }, function (err) {
            should(err).not.be.ok;
          });
        }
      });

      client.socket.on('dealt cards', function (cards) {
        cards.should.have.property(client.nickname).which.is.an.Array;
        cards.should.have.property('briscolaHasBeenDealt').which.is.a.Boolean;

        Array.prototype.push.apply(client.hand, cards[client.nickname]);
      });
      
      client.socket.on('round finished', function (round) {
        client.socket.emit('begin next round');
      });
      
      client.socket.on('match finished', function (match) {
        done();
      });
    }, function () {
      testDidFinish();
    });
  }

  it('should handle players playing', function (done) {
    this.timeout(10000);

    notiyTestDidFinish = done;
  });
});
